module Plugin::SupportTickets::Models
  include BusinessR::Base::Models

  class Ticket < BaseModel
    configure do
      has :workflow
      has :optimistic_locking
      has :versions do |config|
        config.included_relations = [:posts]
      end

      strategy :delete => :soft
     
      many :conversation_items, :references => "Item"
      one  :requester, :references => "::Base::Models::User"
    end
  end

  class Item < BaseModel
    configure do
      one :actor, :references => "::Base::Models::User"
    end
  end
end